====================================
Third-Party Libraries and Components
====================================

The EWARS application ecosystem uses the following third-party libraries and systems in order to accomplish it's features.

Every effort is made to reduce the number of third-party requirements across the application, this is to ensure that applications and components remain small with a lower surface area for bugs/issues arising from complications in third-party libraries and components. In addition to the aforementioned reasons for low-dependency targeting, reducing the number of dependencies reduces risk involving licencing of sub-projects used in the main project.

Some of the components/libraries listed below rely on third-party libraries and components themselves, it would be an undertaking to list them all here so only top-level libraries that EWARS applications import themselves are included in the list below.

Development Pipeline
--------------------

- GNU make
- GCC/LLVM/Mono/Wine
- GNU fswatch
- less compiler
- GNU Bash/Zsh
- Rust, cargo, rustup, rustfmt

Server
------

- Tornado (Python)
- psycopg2 (Python)
- Actix (Python)
- Tokio (Rust)
- boto / boto3 (python)
- serde, serde_json, serde_derive
- postgres-rs

Client
------

- MomentJS
- React
- Numeral
- Highcharts/HighStock
- D3
- Mapboxgl
- Electron

Desktop Server
--------------

- Actix
- tokio
- tokio-uds
- serde, serde_json, serde_derive
- chrono
- rusqlite
- bytes
- imgui-rs, imgui

Mobile
------

- react, react-native
- Android SDK
- Apple iOS SDK
- MomentJS
- NumeralJS
- rusqlite
