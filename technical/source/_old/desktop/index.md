# Desktop Application

The EWARS desktop application is designed to support Windows, macos and Linux runtime environments using a unified single client interface running on top of a low-level socket based server application.

<link rel="stylesheet" type="text/css" href="/static/mermaid/mermaid.css">
<script src="/static/mermaid/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>


## Architecture

<div class="mermaid">
graph TD;
    Desktop-->Electron;
    Electron-->DC(Desktop Client);
    CC(Client)-->DC(Desktop Client);
    Desktop-->SS(Desktop Server);
    SLIB(libsonoma)-->SS(Desktop Server);
</div>
