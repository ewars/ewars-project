# Overall Architecture

EWARS is a multi-language, multi-platform ecosystem of applications and tools which come together to provide reporting, analysis and data management capabilities across a broad spectrum of contextualizations.

<link rel="stylesheet" type="text/css" href="/static/mermaid/mermaid.css">
<link rel="stylesheet" type="text/css" href="/static/mermaid/mermaid.dark.css">
<script src="/static/mermaid/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>

## EcoSystem

<div class="mermaid">
graph TD;
    EWARS-->Desktop;
    Desktop-->Windows;
    Desktop-->macos;
    Desktop-->Linux;
    EWARS-->Mobile;
    Mobile-->Android;
    Mobile-->iOS;
    Mobile-->J2ME;
    EWARS-->Hub;
    EWARS-->Server;
    Server-->NOT_WRK[Notification Worker];
    Server-->ALA_WRK[Alarm Worker];
    Server-->SMS_WRK[SMS Worker];
    Server-->HOK_WRK[Hook Worker];
    Server-->SYN_WRK[Sync Worker];
</div>


## Client Interfaces 

<div class="mermaid">
graph TD;
    Client-->WB[Web Client];
    Client-->DC[Desktop Client];
    Client-->MB[Mobile Client];
    IMGUI-->DC;
</div>

