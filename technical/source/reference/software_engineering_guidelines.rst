JDU S&C Software Engineering Handbook
=======================================

.. csv-table::
   :header: "", ""

   "Version", "0.0.2"
   "Last Updated", "2019-01-01"

Welcome
---------

Welcome to the NASA Software Engineering Handbook (SWEHB). This wiki based Handbook provides users and practitioners with guidance material for implementing the requirements of NPR 7150.2, NASA Software Engineering Requirements. Use of this SWEHB is intended to provide "best-in-class" guidance for the implementation of safe and reliable software in support of NASA projects. This SWEHB is a key component of the NASA Software Working Group's (SWG) implementation of an Agency-wide plan to work toward a continuous and sustained software engineering process and product improvement.

The SWG designed this Handbook for the community that is involved in the acquisition, management, development, assurance, maintenance, and operations of NASA software. Readers can use it to sharpen their skills in specific areas or suggest valuable guidance for others in the NASA software community. Novice and experienced software team members can use the Handbook as an easily accessible reference or manual that captures the broad knowledge base of numerous experts who have extensive experience in all aspects of NASA's software systems.

In this SWEHB you will see information for determining the scope and applicability of the individual requirements from NPR 7150.2. You will also see the rationale behind the requirements, guidance on their implementation, the specific tools that were used in the development of NASA software, pointers to key lessons learned, and select references for further information.

We have adopted the "wiki" approach for this version of the Handbook to encourage you to submit candidate improvements to the information in this Handbook. Your comments, suggestions for improvement, offerings of additional candidate material for the Handbook, and identification of errors are solicited to make this a living and ongoing source of useful information. You can submit your inputs and responses via "Feedback" in the NASA Technical Standards System (NTSS) at http://standards.nasa.gov/.

The SWG's SWEHB Development Team will review and disposition your comments to enhance the wealth of useful material that is now at the fingertips of NASA's software community.

We hope you will find the information helpful in your day-to-day quest for engineering excellence. It has been provided by many contributing experts, distilled into useful chunks by the SWEHB Development Team, and reviewed by NASA's Software community.
