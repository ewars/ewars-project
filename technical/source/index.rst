EWARS Technical Documentation
=============================


.. toctree::
   :numbered:
   :maxdepth: 6 

   next_gen/index
   current_gen/index
   licensing
   third_party
   governance
   contributing

Other Sites
-----------

* `EWARS/EWARS User Guidance <http://docs.ewars.ws>`_
* `EWARS SaaS Application <https://ewars.ws>`_
* `EWARS Project Site <http://ewars-project.org>`_
